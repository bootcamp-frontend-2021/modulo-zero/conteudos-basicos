# Conteúdos Básicos

Repositório da etapa zero do Bootcamp IGTI de Front-end

## Capítulo 01
Somente instalações e preparação de ambiente

## Capítulo 02
Básicos de HTML. Itens legais:
- _colspan_ e _rowspan_
- Parâmetro pra _input_: _readonly_ 
## Capítulo 03
Básicos de CSS
- combinação de descendência: espaço e ">". O primeiro para todo descendente, o segundo para filho direto
(seção 5 do material)
- combinação de irmãos:
    - E ~ F: F é irmão de E precedido por E
    - E + F: F é irmão de E imediatamente precedido por E
- Pseudoclasses no material
- Box sizing: border-box. Bem útil
- Margin laterais para alinhar block-boxes (div etcs)
- Flexbox: Aula 5 do capítulo. Bem semelhante ao fxFlex do Angular
- flex-grow e flex-shrink: ajuste responsivo de tamanho do objetos em uma flexbox

## Capítulo 04
Básico

## Capítulo 05
- document
    - getElementById
    - addEventListener:
        O terceiro parâmetro define em que fase da propagação o evento será capturado. 
        ```js
            document.addEventListener("click", myHandler, false) // bubbling
            document.addEventListener("click", myHandler, false) // capturing
        ```

    - removeEventListener
- Propagação de eventos:
    - Fase de *capturing*: 
        Ocorre de fora pra dentro nas *div*
    - Fase de *bubbling*:
        Ocorre de dentro pra fora de onde o evento ocorreu

- Objeto Evento
  - **event.stopPropagation:** interrompe a paropagação no momento em que é chamada
  - **event.preventDefault:** o navegador não executa a ação padrão (por exemplo, *submmit*)
- Criação de elementos no *DOM*
  - **fieldset:** Conjunto de campos do HTML
  - **createElement:** Criação de elementos no *DOM*
  - **appendChild:** Insere o elemento no HTML amarrado como filho de outro elemento 
  - **querySelector:** Pode ser usado abaixo de elementos, além de a partir do *document*
- Elementos dinâmicos (css + js)
  - **className vs classList.add:** O primeiro seta a classe do elemento HTML, enquato o segundo só adiciona uma classe 
  - **classList.remove:** Remove a classe passada de parâmetro
  - **clientX e clientY:** Relativos ao viewport do navegador
## Capítulo 06
- js pre-moderno
  - *this* funciona à partir do objeto alvo, e não pela sua declaração (exemplo na aula 01)
  - utilidade do *bind* quando essas funções são acionadas via elemento do *DOM*, por exemplo
  - **.prototype:** utilizado para compartilhamento de memória em caso de, por exemplo, funções padrão de objetos.
- js-moderno
  - **Palavras chave:** *class*, *constructor*, *extends* (bem semellhante ao *typescript*), *super*
  - Nesses casos não é necessário usar o *prototype*. Já está encapsulado na sintaxe de *class*
  - Superposição de funções filhasfunciona como em outras linguagens

## Capítulo 07
- **var vs let:**
  - *var:* todo o escopo declarado;
  - *let:* somente no bloco declarado
- **Declaração por desestruturação:** operador *rest* (...) para separação dos objetos em um vetor:
  ```js
  let primos = [2, 3, 5, 7, 11, 13];
  let [p1, p2, ...resto] = primos;
  ``` 
  Também pode ser utilizado para objetos:
  ```js
  let curso = {
    nome: "Bootcamp Front End",
    modulos: 4,
    presencial: false,
    turma: 1,
  };
  let { nome: nomeCurso, turma, ...outrosCampos } = curso;
  ```
  E além disso a desestruturação pode ser usada também como parâmetros de função:
  ```js
  function imprime({ nome }) {
    //em caso de objetos como no item anterior (curso)
    console.log(nome);
    }
  ```
- **Spread Operator:** Criação de elementos hetergêneos à partir de múltiplos objetos
    ```js
    let primos = [2, 3, 5, 7, 11, 13];

    let curso = {
        nome: "Bootcamp Front End",
        modulos: 4,
        presencial: false,
        turma: 1,
    };

    let primos2 = [...primos, 17];
    let primos3 = [1, ...primos, 17];
    ```
    Também é possível utilizar esse operador para objetos:
    ```js
    let curso2 = {
        ...curso,
        descricao: "Bla bla bla",
        ativo: true
    }

    let curso3 = {
        ...curso
    };
    ```
- **Template literals:** Uso do caracter *crase* e do caracter *$* para construção de uma string. Também possibilita contemplar quebras de linha e outros caracteres que seriam inválidos em strings comuns:
    ```js
    let a = 2, b = 3;
    let soma = a + b;

    console.log(a + " + " + b + " \n= " + soma);

    console.log(`${a} + ${b} 
    = ${soma}`);
    ```
## Capítulo 08
- **async await:** Toda função *async* retorna automaticamente uma promise, e todo *await* usado dentro dela é como um encadeamento de *.then*
- **try_catch:** É possível usar *try_catch* quando temos o uso de *async_await*

        O DESAFIO NÃO FOI FEITO. BORA FAZER

## Capítulo 09